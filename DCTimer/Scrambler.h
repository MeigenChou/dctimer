//
//  Scrambler.h
//  Button Fun
//
//  Created by  on 12-11-2.
//  Copyright (c) 2012年 Dave Mark. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Scrambler : NSObject
- (NSString *)getScrString: (int)idx;
- (NSString *)solveCross: (NSString *)scr side:(int)side;
- (NSString *)solveXcross:(NSString *)scr side:(int)side;
- (NSString *)solveEoline:(NSString *)scr side:(int)side;
- (NSString *) imageString:(int)size scr:(NSString *)scr;
- (void) initSq1;
- (int) viewType;
@end
