//
//  DCTFirstViewController.m
//  DCTimer
//
//  Created by MeigenChou on 13-3-2.
//  Copyright (c) 2013年 MeigenChou. All rights reserved.
//

#import "DCTFirstViewController.h"
#import "CustomPickerView.h"
#import "Scrambler.h"
#import "DCTMethodsImpl.h"
#import "DCTDbHelper.h"
#import "DCTImageViewController.h"
#import "UIViewController+MJPopupViewController.h"
#import "QuartzCore/QuartzCore.h"
#import <mach/mach_time.h>

@interface DCTFirstViewController()
@property (nonatomic, strong) Scrambler *scrambler;
@property (nonatomic, strong) CustomPickerView *alert;
@property (nonatomic, strong) DCTMethodsImpl *mi;
@property (nonatomic, strong) DCTDbHelper *dbh;
@property (nonatomic, strong) NSString *currentScr;
@end

@implementation DCTFirstViewController
@synthesize btnScrView;
@synthesize scrLabel;
@synthesize timerLabel = _timerLabel;
@synthesize btnScrType;
@synthesize scrambler = _scrambler;
@synthesize alert = _alert;
@synthesize currentScr = _currentScr;
@synthesize mi = _mi;
@synthesize dbh = _dbh;
@synthesize gestureStartPoint;
int swipeType;
bool isChange = true;
int timerState; //0-停止 1-计时中 2-观察中
int inspState;  //2-观察 2-+2 3-DNF
mach_timebase_info_data_t info;
uint64_t timeStart;
uint64_t timeEnd;
NSTimer *dctTimer, *fTimer, *inspTimer;
int resTime;
NSDateFormatter *formatter;
bool canStart;
int fTime;
bool wcaInsp, wcaInst;
extern int timerupd;
extern int accuracy;
extern bool clkFormat;
extern bool promTime;
extern int cside;
extern int cxe;
extern bool prntScr;
NSString *extsol;
int currentSesIdx;
NSString *lastScr = @"";
int crntScrType;
int bgcolor, textcolor;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Timer", @"Timer");
        self.tabBarItem.image = [UIImage imageNamed:@"img1"];
        mach_timebase_info(&info);
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    }
    return self;
}

- (Scrambler *)scrambler {
    if(!_scrambler)
        _scrambler = [[Scrambler alloc] init];
    return _scrambler;
}

- (DCTMethodsImpl *)mi {
    if(!_mi) 
        _mi = [[DCTMethodsImpl alloc] init];
    return _mi;
}

- (DCTDbHelper *)dbh {
    if(!_dbh)
        _dbh = [[DCTDbHelper alloc] init];
    return _dbh;
}

- (CustomPickerView *) alert {
    if(!_alert) {
        _alert = [[CustomPickerView alloc]
                  initWithTitle:NSLocalizedString(@"ScrType", @"")
                  message:nil
                  delegate:self
                  cancelButtonTitle:NSLocalizedString(@"cancel", @"")
                  otherButtonTitles:NSLocalizedString(@"done", @""), nil];
    }
    return _alert;
}
							
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
- (void) setScrLblFont {
    int type = crntScrType >> 5;
    int sub = crntScrType & 31;
    NSLog(@"%d %d", type, sub);
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        if(type==0 || type==7 || type==13 || type==15)
            [self.scrLabel setFont:[UIFont fontWithName:@"Arial" size:30]];
        else if(type==1 || type==2)
            [self.scrLabel setFont:[UIFont fontWithName:@"Arial" size:28]];
        else [self.scrLabel setFont:[UIFont fontWithName:@"Arial" size:25]];
    }
    else if(type==0 || type==7 || type==13 || type==15)
        [self.scrLabel setFont:[UIFont fontWithName:@"Arial" size:19]];
    else if(type==3)
        [self.scrLabel setFont:[UIFont fontWithName:@"Arial" size:14]];
    else if(type==4)
        [self.scrLabel setFont:[UIFont fontWithName:@"Arial" size:13]];
    else if(type==5) {
        if(sub==0)
            [self.scrLabel setFont:[UIFont fontWithName:@"Arial" size:11.5f]];
        else [self.scrLabel setFont:[UIFont fontWithName:@"Arial" size:12]];
    }
    else if(type==6)
        [self.scrLabel setFont:[UIFont fontWithName:@"Arial" size:12]];
    else if(type==16 && sub==3)
        [self.scrLabel setFont:[UIFont fontWithName:@"Arial" size:13]];
    else [self.scrLabel setFont:[UIFont fontWithName:@"Arial" size:17]];
}

- (void) newScramble {
    lastScr = self.currentScr;
    int type = crntScrType >> 5;
    int sub = crntScrType & 31;
    self.currentScr = [self.scrambler getScrString:crntScrType];
    if(type==1 && sub<2 && cxe != 0) {
        if(cxe==1)
            extsol = [self.scrambler solveCross:self.currentScr side:cside];
        else if(cxe==2)
            extsol = [self.scrambler solveXcross:self.currentScr side:cside];
        else if(cxe==3)
            extsol = [self.scrambler solveEoline:self.currentScr side:cside];
        scrLabel.text = [NSString stringWithFormat:@"%@\n%@", self.currentScr, extsol];
    }
    else scrLabel.text = self.currentScr;
}

- (void)extraSolve {
    int type = crntScrType >> 5;
    int sub = crntScrType & 31;
    if(type==1 && sub<2 && cxe != 0) {
        if(cxe==1)
            extsol = [self.scrambler solveCross:self.currentScr side:cside];
        else if(cxe==2)
            extsol = [self.scrambler solveXcross:self.currentScr side:cside];
        else if(cxe==3)
            extsol = [self.scrambler solveEoline:self.currentScr side:cside];
        scrLabel.text = [NSString stringWithFormat:@"%@\n%@", self.currentScr, extsol];
    }
    else scrLabel.text = self.currentScr;
}

- (void) loadDefaults {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    fTime = [[defaults objectForKey:@"freezeslide"] intValue];
    wcaInst = [defaults boolForKey:@"wcainsp"];
    if(timerState == 0)wcaInsp = wcaInst;
    timerupd = [defaults integerForKey:@"timerupd"];
    clkFormat = [defaults boolForKey:@"clockform"];
    accuracy = [defaults integerForKey:@"accuracy"];
    promTime = [defaults boolForKey:@"prompttime"];
    cxe = [defaults integerForKey:@"cxe"];
    cside = [defaults integerForKey:@"cside"];
    prntScr = [defaults boolForKey:@"printscr"];
    currentSesIdx = [defaults integerForKey:@"crntsesidx"];
    crntScrType = [defaults integerForKey:@"crntscrtype"];
    bgcolor = [defaults integerForKey:@"bgcolor"];
    textcolor = [defaults integerForKey:@"textcolor"];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadDefaults];
    if(accuracy == 1)self.timerLabel.text = @"0.00";
    [btnScrView setTitle:NSLocalizedString(@"ScrView", @"") forState:UIControlStateNormal];
    [self.scrambler getScrString:32];
    [self.scrambler initSq1];
    int type = crntScrType>>5;
    int sub = crntScrType&31;
    self.currentScr = [self.scrambler getScrString:crntScrType];
    if(type==1 && sub<2 && cside != 6) {
        extsol = [self.scrambler solveXcross:self.currentScr side:cside];
        scrLabel.text = [NSString stringWithFormat:@"%@\n%@", self.currentScr, extsol];
    }
    else scrLabel.text = self.currentScr;
    lastScr = self.currentScr;
    [btnScrType setTitle:[self.alert getScrName:crntScrType] forState:UIControlStateNormal];
    [self setScrLblFont];
    UIImage *btnImageNormal = [UIImage imageNamed:@"whiteButton.png"];
    UIImage *sbtnImageNormal = [btnImageNormal stretchableImageWithLeftCapWidth:12 topCapHeight:0];
    [btnScrType setBackgroundImage:sbtnImageNormal forState:UIControlStateNormal];
    [btnScrView setBackgroundImage:sbtnImageNormal forState:UIControlStateNormal];
    btnImageNormal = [UIImage imageNamed:@"blueButton.png"];
    sbtnImageNormal = [btnImageNormal stretchableImageWithLeftCapWidth:12 topCapHeight:0];
    [btnScrType setBackgroundImage:sbtnImageNormal forState:UIControlStateHighlighted];
    [btnScrView setBackgroundImage:sbtnImageNormal forState:UIControlStateHighlighted];
    [self.dbh getSessions];
    [self.dbh query:currentSesIdx];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [self setScrLabel:nil];
    [self setBtnScrType:nil];
    [self setBtnScrView:nil];
    [self setTimerLabel:nil];
    [self.dbh closeDB];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self loadDefaults];
    int r = (bgcolor>>16)&0xff;
    int g = (bgcolor>>8)&0xff;
    int b = bgcolor&0xff;
    [self.view setBackgroundColor:[UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]];
    r = (textcolor>>16)&0xff;
    g = (textcolor>>8)&0xff;
    b = textcolor&0xff;
    self.scrLabel.textColor = [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1];
    self.timerLabel.textColor = [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1];
    [self extraSolve];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
    if(timerState == 2) {
        [inspTimer invalidate];
        self.timerLabel.textColor = [UIColor blackColor];
    }
    else if(timerState == 1) {
        [dctTimer invalidate];
    }
    timerState = 0;
    btnScrType.hidden = NO;
    btnScrView.hidden = NO;
    wcaInsp = wcaInst;
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (void)record:(int)time pen:(int)pen {
    NSDate *date = [NSDate date];
    NSString *nowtimeStr = [formatter stringFromDate:date];
    [self.mi addTime:time penalty:pen scramble:lastScr datetime:nowtimeStr];
    [self.dbh insertTime:time pen:pen scr:lastScr date:nowtimeStr];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}

- (IBAction)selScrambleType:(id)sender {
    [self.alert setTag:0];
    [self.alert show];
}

- (IBAction)drawScrView:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:NSLocalizedString(@"ScrView", @"")
                          message:NSLocalizedString(@"not_support", @"")
                          delegate:self
                          cancelButtonTitle:NSLocalizedString(@"close", @"")
                          otherButtonTitles:nil];
    [alert show];
    /*
    int vt = self.scrambler.viewType;
    if(vt!=0) {
        DCTImageViewController *imageView = [[DCTImageViewController alloc] init];
        [imageView.view setFrame:CGRectMake(0, 0, 250, 180)];
        imageView.view.layer.cornerRadius = 5;
        imageView.view.layer.masksToBounds = YES;
        [self presentPopupViewController:imageView animationType:MJPopupViewAnimationFade];
        NSString *img = [self.scrambler imageString:vt scr:self.currentScr];
        NSLog(@"%@", img);
    } else {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:NSLocalizedString(@"ScrView", @"")
                              message:NSLocalizedString(@"not_support", @"")
                              delegate:self
                              cancelButtonTitle:NSLocalizedString(@"close", @"")
                              otherButtonTitles:nil];
        [alert show];
    }
     */
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (buttonIndex) {
        case 0:
            if(alertView.tag!=0)
                inspState = 1;
            break;
        case 1:
            switch (alertView.tag) {
                case 0:
                {
                    [btnScrType setTitle:self.alert.selScrName forState:UIControlStateNormal];
                    crntScrType = [self.alert.selScrType intValue];
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    [defaults setInteger:crntScrType forKey:@"crntscrtype"];
                    [self newScramble];
                    [self setScrLblFont];
                    break;
                }
                case 1:
                {
                    int time = inspState==2?resTime+2000:resTime;
                    [self record:time pen:0];
                    inspState = 1;
                    break;
                }
                case 2:
                    [self record:resTime pen:2];
                    inspState = 1;
                    break;
                case 3:
                    [self.mi deleteTimeAtIndex:[self.mi numberOfSolves]-1];
                    [self.dbh deleteTime:[self.mi numberOfSolves]];
                    break;
            }
            break;
        case 2:
        {
            int time = inspState==2?resTime+2000:resTime;
            [self record:time pen:1];
            inspState = 1;
            break;
        }
        case 3:
        {
            int time = inspState==2?resTime+2000:resTime;
            [self record:time pen:2];
            inspState = 1;
            break;
        }
        default:
            break;
    }
}

- (NSString *)contime:(int)i {
    bool m = i<0;
    if(m)i = -i;
    i/=1000;
    int sec=clkFormat?i%60:i;
    int min=clkFormat?(i/60)%60:0;
    int hour=clkFormat?i/3600:0;
    NSMutableString *s = [NSMutableString string];
    if(hour==0) {
        if(min==0) [s appendFormat:@"%d", sec];
        else {
            if(sec<10) [s appendFormat:@"%d:0%d", min, sec];
            else [s appendFormat:@"%d:%d", min, sec];
        }
    } else {
        [s appendFormat:@"%d", hour];
        if(min<10) [s appendFormat:@":0%d", min];
        else [s appendFormat:@":%d", min];
        if(sec<10) [s appendFormat:@":0%d", sec];
        else [s appendFormat:@":%d", sec];
    }
    return s;
}

- (void)updateTime {
    uint64_t timeNow = mach_absolute_time();
    int time = (timeNow - timeStart) * info.numer / info.denom / 1000000;
    if(timerupd == 0) self.timerLabel.text = distime(time);
    else if(timerupd == 1) self.timerLabel.text = [self contime:time];
    else self.timerLabel.text = NSLocalizedString(@"solve", @"");
}

- (void)updateInspTime {
    uint64_t timeNow = mach_absolute_time();
    int time = (timeNow - timeStart) * info.numer / info.denom / 1000000;
    if(time/1000<15) {
        int sec = (15 - time/1000);
        if(timerupd < 3) self.timerLabel.text = [NSString stringWithFormat:@"%d", sec];
        else self.timerLabel.text = NSLocalizedString(@"inspect", @"");
        inspState = 1;
    } else if(time/1000<17) {
        if(timerupd < 3) self.timerLabel.text = @"+2";
        inspState = 2;
    } else {
        if(timerupd < 3) self.timerLabel.text = @"DNF";
        inspState = 3;
    }
}

- (void)setCanStart {
    canStart = true;
    self.timerLabel.textColor = [UIColor greenColor];
}

#pragma mark -
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    gestureStartPoint = [touch locationInView:self.view];
    switch (timerState) {
        case 0:
            if(fTime == 0 || wcaInsp) {
                canStart = true;
                self.timerLabel.textColor = [UIColor greenColor];
            } else {
                canStart = false;
                self.timerLabel.textColor = [UIColor redColor];
                fTimer = [NSTimer scheduledTimerWithTimeInterval:fTime*0.05 target:self selector:@selector(setCanStart) userInfo:nil repeats:NO];
            }
            break;
        case 1:
        {
            timeEnd = mach_absolute_time();
            [dctTimer invalidate];
            resTime = (timeEnd - timeStart) * info.numer / info.denom / 1000000;
            self.timerLabel.text = distime(resTime);
            btnScrType.hidden = NO;
            btnScrView.hidden = NO;
            [[UIApplication sharedApplication]setIdleTimerDisabled:NO];
            break;
        }
        default:
            if(fTime == 0) {
                canStart = true;
                self.timerLabel.textColor = [UIColor greenColor];
            } else {
                canStart = false;
                self.timerLabel.textColor = [UIColor yellowColor];
                fTimer = [NSTimer scheduledTimerWithTimeInterval:fTime*0.05 target:self selector:@selector(setCanStart) userInfo:nil repeats:NO];
            }
            break;
    }
    swipeType = 0;
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    self.timerLabel.textColor = [UIColor blackColor];
    timerState = 0;
    inspState = 1;
    swipeType = 0;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    self.timerLabel.textColor = [UIColor blackColor];
    if(swipeType == 1) {
        isChange = true;
        [fTimer invalidate];
    }
    else if(swipeType == 2) {
        isChange = true;
        [fTimer invalidate];
        if([self.mi numberOfSolves]>0) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"deletelast", @"") message:nil delegate:self cancelButtonTitle:NSLocalizedString(@"cancel", @"") otherButtonTitles:NSLocalizedString(@"ok", @""), nil];
            [alert setTag:3];
            [alert show];
        }
    }
    else {
        switch (timerState) {
            case 0:
                if(canStart) {
                    timeStart = mach_absolute_time();
                    if(wcaInsp) {
                        self.timerLabel.textColor = [UIColor redColor];
                        inspTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateInspTime) userInfo:nil repeats:YES];
                        timerState = 2;
                    } else {
                        dctTimer = [NSTimer scheduledTimerWithTimeInterval:0.017 target:self selector:@selector(updateTime) userInfo:nil repeats:YES];
                        timerState = 1;
                    }
                    btnScrType.hidden = YES;
                    btnScrView.hidden = YES;
                    [[UIApplication sharedApplication]setIdleTimerDisabled:YES];
                } else {
                    [fTimer invalidate];
                    self.timerLabel.textColor = [UIColor blackColor];
                }
                break;
            case 1:
            {
                [self newScramble];
                timerState = 0;
                wcaInsp = wcaInst;
                if(promTime) {
                    int time = (inspState==2)?resTime+2000:resTime;
                    if(inspState==3) {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%@: DNF", NSLocalizedString(@"time", @"")] message:@"" delegate:self cancelButtonTitle:NSLocalizedString(@"cancel", @"") otherButtonTitles:NSLocalizedString(@"save", @""), nil];
                        [alert setTag:2];
                    } else {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"time", @""), distime(time)] message:@"" delegate:self cancelButtonTitle:NSLocalizedString(@"cancel", @"") otherButtonTitles:NSLocalizedString(@"nopen", @""), @"+2", @"DNF", nil];
                        [alert setTag:1];
                        [alert show];
                    }
                } else {
                    int time = inspState==2?resTime+2000:resTime;
                    int pen = inspState==3?2:0;
                    [self record:time pen:pen];
                    inspState = 1;
                }
                //self.timerLabel.text = @"0.000";
                
                break;
            }
            case 2:
                if(canStart) {
                    timeStart = mach_absolute_time();
                    [inspTimer invalidate];
                    dctTimer = [NSTimer scheduledTimerWithTimeInterval:0.017 target:self selector:@selector(updateTime) userInfo:nil repeats:YES];
                    timerState = 1;
                } else {
                    [fTimer invalidate];
                    self.timerLabel.textColor = [UIColor redColor];
                }
                break;
        }
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    //self.timerLabel.textColor = [UIColor blackColor];
    if(timerState == 0) {
        UITouch *touch = [touches anyObject];
        CGPoint currentPo = [touch locationInView:self.view];
        CGFloat deltaX = currentPo.x - gestureStartPoint.x;
        CGFloat deltaY = currentPo.y - gestureStartPoint.y;
        if(fabsf(deltaY) <= 8) {
            if(deltaX >= 25) {
                swipeType = 1;
                if(isChange) {
                    self.timerLabel.textColor = [UIColor blackColor];
                    [fTimer invalidate];
                    [self newScramble];
                    isChange = false;
                }
            }
            else if(deltaX <= -25) {
                swipeType = 2;
                if(isChange) {
                    self.timerLabel.textColor = [UIColor blackColor];
                    [fTimer invalidate];
                    isChange = false;
                }
            }
        }
    }
}
@end
