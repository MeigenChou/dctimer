//
//  DCTDetailViewController.m
//  DCTimer
//
//  Created by Chou Meigen on 13-3-19.
//  Copyright (c) 2013年 __MyCompanyName__. All rights reserved.
//

#import "DCTDetailViewController.h"
#import "DCTMethodsImpl.h"
#import "DCTDbHelper.h"

@interface DCTDetailViewController()
@property (nonatomic, strong) DCTMethodsImpl *mi;
@property (nonatomic, strong) DCTDbHelper *dbh;
@end

@implementation DCTDetailViewController
@synthesize lbTime;
@synthesize lbScr;
@synthesize time;
@synthesize scramble;
@synthesize resi;
@synthesize mi = _mi;
@synthesize dbh = _dbh;
int bgcolor, textcolor;

NSMutableArray *resInfo;

- (void)viewWillAppear:(BOOL)animated {
    int r = (bgcolor>>16)&0xff;
    int g = (bgcolor>>8)&0xff;
    int b = bgcolor&0xff;
    [self.view setBackgroundColor:[UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]];
    r = (textcolor>>16)&0xff;
    g = (textcolor>>8)&0xff;
    b = textcolor&0xff;
    lbTime.text = time;
    lbScr.text = scramble;
    [lbTime setTextColor:[UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]];
    [lbScr setTextColor:[UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]];
    resInfo = [[NSMutableArray alloc] initWithObjects:[resi objectAtIndex:0], [resi objectAtIndex:1], nil];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"title_more.png"] style:UIBarButtonItemStylePlain target:self action:@selector(displayActionSheet)];
    [super viewWillAppear:animated];
}

- (void)viewDidUnload {
    self.time = nil;
    self.scramble = nil;
    self.lbTime = nil;
    self.lbScr = nil;
    self.resi = nil;
    [super viewDidUnload];
}

- (DCTMethodsImpl *)mi {
    if(!_mi) 
        _mi = [[DCTMethodsImpl alloc] init];
    return _mi;
}

- (DCTDbHelper *)dbh {
    if(!_dbh)
        _dbh = [[DCTDbHelper alloc] init];
    return _dbh;
}

- (void) displayActionSheet {
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
        initWithTitle:NSLocalizedString(@"option", @"") delegate:self
        cancelButtonTitle:NSLocalizedString(@"cancel", @"")
        destructiveButtonTitle:NSLocalizedString(@"delete", @"")
        otherButtonTitles:NSLocalizedString(@"nopen", @""), @"+2", @"DNF", NSLocalizedString(@"copyscr", @""), nil];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [actionSheet showFromBarButtonItem:self.navigationItem.rightBarButtonItem animated:YES];
    }
    else [actionSheet showInView:[UIApplication sharedApplication].keyWindow];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    int idx = [[resInfo objectAtIndex:0] intValue];
    NSString *pena = [resInfo objectAtIndex:1];
    NSLog(@"%d %@", idx, pena);
    switch (buttonIndex) {
        case 0: //delete
            [self.mi deleteTimeAtIndex:idx];
            [self.dbh deleteTime:idx];
            [self.navigationController popToRootViewControllerAnimated:YES];
            break;
        case 1: //no penalty
            if(![pena isEqualToString:@"n"]) {
                [self.mi setPenalty:0 atIndex:idx];
                [self.dbh updateTime:idx pen:0];
                self.lbTime.text = [NSString stringWithFormat:@"%@\n(%@)", [self.mi distimeAtIndex:idx dt:true], [self.mi getDateAtIndex:idx]];
                [resInfo replaceObjectAtIndex:1 withObject:@"n"];
            }
            break;
        case 2: //+2
            if(![pena isEqualToString:@"p"]) {
                [self.mi setPenalty:1 atIndex:idx];
                [self.dbh updateTime:idx pen:1];
                self.lbTime.text = [NSString stringWithFormat:@"%@\n(%@)", [self.mi distimeAtIndex:idx dt:true], [self.mi getDateAtIndex:idx]];
                [resInfo replaceObjectAtIndex:1 withObject:@"p"];
            }
            break;
        case 3: //DNF
            if(![pena isEqualToString:@"d"]) {
                [self.mi setPenalty:2 atIndex:idx];
                [self.dbh updateTime:idx pen:2];
                self.lbTime.text = [NSString stringWithFormat:@"%@\n(%@)", [self.mi distimeAtIndex:idx dt:true], [self.mi getDateAtIndex:idx]];
                [resInfo replaceObjectAtIndex:1 withObject:@"d"];
            }
            break;
        case 4: //copy scr
        {
            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
            pasteboard.string = lbScr.text;
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"copysuccess", NULL) delegate:self cancelButtonTitle:NSLocalizedString(@"close", @"") otherButtonTitles:nil];
            [alertView show];
            break;
        }
        default:
            break;
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}
@end
