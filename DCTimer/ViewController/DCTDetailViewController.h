//
//  DCTDetailViewController.h
//  DCTimer
//
//  Created by Chou Meigen on 13-3-19.
//  Copyright (c) 2013年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DCTDetailViewController : UIViewController <UIActionSheetDelegate>
@property (strong, nonatomic) IBOutlet UILabel *lbTime;
@property (strong, nonatomic) IBOutlet UILabel *lbScr;
@property (copy, nonatomic) NSString *time, *scramble;
@property (copy, nonatomic) NSArray *resi;
@end
