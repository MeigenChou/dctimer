//
//  DCTResultViewController.h
//  DCTimer
//
//  Created by Chou Meigen on 13-3-19.
//  Copyright (c) 2013年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DCTResultViewController : UITableViewController
@property (nonatomic, strong) NSMutableArray *listData;
@end
