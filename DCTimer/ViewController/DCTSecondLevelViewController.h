//
//  DCTSecondLevelViewController.h
//  DCTimer
//
//  Created by Chou Meigen on 13-3-20.
//  Copyright (c) 2013年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DCTSecondLevelViewController : UITableViewController
@property (copy, nonatomic) NSArray *array;
@property (copy, nonatomic) NSNumber *selIndex;
@property (copy, nonatomic) NSString *key;
@end
