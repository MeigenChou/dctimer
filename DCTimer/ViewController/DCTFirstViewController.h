//
//  DCTFirstViewController.h
//  DCTimer
//
//  Created by MeigenChou on 13-3-2.
//  Copyright (c) 2013年 MeigenChou. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DCTFirstViewController : UIViewController <UIAlertViewDelegate>
@property (strong, nonatomic) IBOutlet UIButton *btnScrView;
@property (strong, nonatomic) IBOutlet UILabel *scrLabel;
@property (strong, nonatomic) IBOutlet UILabel *timerLabel;
@property (strong, nonatomic) IBOutlet UIButton *btnScrType;
@property CGPoint gestureStartPoint;

- (IBAction)selScrambleType:(id)sender;
- (IBAction)drawScrView:(id)sender;

@end
