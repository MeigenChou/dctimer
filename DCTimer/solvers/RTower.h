//
//  RTower.h
//  DCTimer solvers
//
//  Created by MeigenChou on 13-3-17.
//  Copyright (c) 2013年 MeigenChou. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RTower : NSObject
- (NSString *)scramble;
@end
