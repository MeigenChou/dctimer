//
//  Floppy.m
//  DCTimer solvers
//
//  Created by MeigenChou on 13-3-14.
//  Copyright (c) 2013年 MeigenChou. All rights reserved.
//

#import "Floppy.h"
#import "Im.h"
#import "stdlib.h"
#import "time.h"

@implementation Floppy

char distance[24][16];
NSArray *turn;

- (void) initf {
    for (int i = 0; i < 24; i++)
        for (int j = 0; j < 16; j++)
            distance[i][j] = -1;
    distance[0][0] = 0;
    int nVisited = 1;
    int depth = 0;
    int cp[4];
    int eo[4];
    while (nVisited > 0) {
        nVisited = 0;
        for (int i = 0; i < 24; i++)
            for (int j = 0; j < 16; j++)
                if (distance[i][j] == depth)
                    for(int k=0; k<4; k++) {
                        idxToPerm(cp, i, 4);
                        idxToOri(eo, j, 2, 4);
                        switch(k){
                            case 0:
                                cir3(cp, 0, 1); break;
                            case 1:
                                cir3(cp, 1, 2); break;
                            case 2:
                                cir3(cp, 2, 3); break;
                            case 3:
                                cir3(cp, 0, 3); break;
                        }
                        eo[k]=1-eo[k];
                        int cpi = permToIdx(cp, 4);
                        int eoi = oriToIdx(eo, 2, 4);
                        if (distance[cpi][eoi] == -1) {
                            distance[cpi][eoi] = depth + 1;
                            nVisited++;
                        }
                    }
        depth++;
    }
}

- (id) init {
    if(self = [super init]) {
        turn = [[NSArray alloc] initWithObjects:@"U ", @"R ", @"D ", @"L ", nil];
        [self initf];
        srand((unsigned)time(0));
    }
    return self;
}

- (NSString *) scrFlopy {
    for (;;) {
        int cpi = rand()%24;
        int eoi = rand()%16;
        if (distance[cpi][eoi] > 0) {
            NSMutableString *sb = [NSMutableString string];
            while(distance[cpi][eoi] != 0){
                int cp[4];
                int eo[4];
                for (int i=0; i<4; i++) {
                    idxToPerm(cp, cpi, 4);
                    idxToOri(eo, eoi, 2, 4);
                    switch(i) {
                        case 0:
                            cir3(cp, 0, 1); break;
                        case 1:
                            cir3(cp, 1, 2); break;
                        case 2:
                            cir3(cp, 2, 3); break;
                        case 3:
                            cir3(cp, 0, 3); break;
                    }
                    eo[i]=1-eo[i];
                    int nextCpi = permToIdx(cp, 4);
                    int nextEoi = oriToIdx(eo, 2, 4);
                    if (distance[nextCpi][nextEoi] == distance[cpi][eoi] - 1) {
                        [sb insertString:[turn objectAtIndex:i] atIndex:0];
                        cpi = nextCpi;
                        eoi = nextEoi;
                        break;
                    }
                }
            }
            return sb;
        }
    }
}

@end
