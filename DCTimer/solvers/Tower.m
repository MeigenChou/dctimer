//
//  Tower.m
//  DCTimer Solvers
//
//  Created by MeigenChou on 12-11-3.
//  Copyright (c) 2012年 MeigenChou. All rights reserved.
//

#import "Tower.h"
#import "Im.h"
#import "stdlib.h"
#import "time.h"

@interface Tower()

@end

@implementation Tower
unsigned short towCpm[40320][6];
unsigned short towEpm[24][6];
char towCpd[40320];
char towEpd[24];
char faces[] = {3,3,1,1,1,1};

NSArray *turnTow;
NSArray *suffTow;
NSMutableArray *solTow;
bool iniTow;

- (Tower *) init {
    if(self = [super init]) {
        turnTow = [[NSArray alloc] initWithObjects:@"U", @"D", @"L", @"R", @"F", @"B", nil];
        suffTow = [[NSArray alloc] initWithObjects:@"'", @"2", @"", nil];
        solTow = [[NSMutableArray alloc] init];
        srand((unsigned)time(0));
    }
    return self;
}

- (void) initTower {
    if(iniTow)return;
    initIm();
    int arr[8];
    int i, j;
    for(i=0; i<40320; i++) {
        for(j=0; j<6; j++) {
            set8Perm(arr, i);
            switch (j) {
                case 0:
                    cir(arr, 0, 3, 2, 1); break;    //U
                case 1:
                    cir(arr, 4, 5, 6, 7); break;	//D
                case 2:
                    cir2(arr, 0, 7, 3, 4); break;	//L
                case 3:
                    cir2(arr, 1, 6, 2, 5); break;	//R
                case 4:
                    cir2(arr, 3, 6, 2, 7); break;	//F
                case 5:
                    cir2(arr, 0, 5, 1, 4); break;	//B
            }
            towCpm[i][j] = get8Perm(arr);
        }
    }
    for(i=1; i<40320; i++) towCpd[i] = -1;
    towCpd[0] = 0;
    int c;
    for(int d=0; d<13; d++) {
        //c = 0;
        for(i=0; i<40320; i++)
            if(towCpd[i] == d)
                for(int k=0; k<6; k++) {
                    int y = i;
                    for(int m=0; m<faces[k]; m++) {
                        y = towCpm[y][k];
                        if(towCpd[y] < 0) {
                            towCpd[y] = d+1;
                            //c++;
                        }
                    }
                }
        //NSLog(@"%@", [NSString stringWithFormat:@"%d", c]);
    }
    
    for(i=0; i<24; i++) {
        for(j=0; j<6; j++) {
            if(j<2) towEpm[i][j]=i;
            else {
                idxToPerm(arr, i, 4);
                switch (j) {
                    case 2:
                        cir3(arr, 0, 3); break;	//L
                    case 3:
                        cir3(arr, 1, 2); break;	//R
                    case 4:
                        cir3(arr, 3, 2); break;	//F
                    case 5:
                        cir3(arr, 1, 0); break;	//B
                }
                towEpm[i][j] = permToIdx(arr, 4);
            }
        }
    }
    
    for(i=1; i<24; i++) towEpd[i] = -1;
    towEpd[0] = 0;
    for(int d=0; d<4; d++) {
        c=0;
        for(i=0; i<24; i++) {
            if(towEpd[i] == d) {
                for(int k=2; k<6; k++) {
                    int next = towEpm[i][k];
                    if(towEpd[next] < 0) {
                        towEpd[next] = d+1;
                        c++;
                    }
                }
            }
        }
        //NSLog(@"%@", [NSString stringWithFormat:@"%d", c]);
    }
    iniTow=true;
}

- (BOOL) searchTow: (int)cp ep:(int)ep d:(int)d lf:(int)lf {
    if(d==0) return cp==0 && ep==0;
    if(towCpd[cp]>d || towEpd[ep]>d)return NO;
    int y, s;
    for(int i=0; i<6; i++)
        if(i!=lf) {
            y=cp; s=ep;
            for(int k=0; k<faces[i]; k++) {
                y=towCpm[y][i]; s=towEpm[s][i];
                if([self searchTow:y ep:s d:(d-1) lf:i]){
                    [solTow addObject:[NSNumber numberWithInt:(i*4 + k)]];
                    return YES;
                }
            }
        }
    return NO;
}

- (NSString *) scrTow {
    [self initTower];
    int cp = rand() % 40320;
    int ep = rand() % 24;
    [solTow removeAllObjects];
    NSString *s = @"";
    for(int d=0; ![self searchTow:cp ep:ep d:d lf:-1]; d++);
    for (int q=0; q<solTow.count; q++) {
        int i=[[solTow objectAtIndex:q] intValue];
        //NSLog(@"%@", [NSString stringWithFormat:@"%d", i]);
        NSString *temp1 = [turnTow objectAtIndex:(i/4)];
        NSString *temp2 = i/4<2?[suffTow objectAtIndex:(i%4)]:@"2";
        //NSLog(@"%@", [temp1 stringByAppendingString:temp2]);
        s = [s stringByAppendingFormat:@"%@%@ ", temp1, temp2];
    }
    //NSLog(@"%@", s);
    return s;
}
@end
