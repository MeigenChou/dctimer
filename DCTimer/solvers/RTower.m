//
//  RTower.m
//  DCTimer solvers
//
//  Created by MeigenChou on 13-3-17.
//  Copyright (c) 2013年 MeigenChou. All rights reserved.
//

#import "RTower.h"
#import "Im.h"
#import "stdlib.h"
#import "time.h"

@implementation RTower
unsigned short cpmr[40320][4];
unsigned short epmr[40320][4];
unsigned short eomr[2187][5];
char cpdr[40320];
char epdr[40320];
char eodr[2187];
int facesr[] = {1,1,3,3};
NSArray *turnr1, *turnr2;
NSArray *suffr;
int seqr[35];
int lenr1;

- (id) init {
    if(self = [super init]) {
        turnr1 = [[NSArray alloc] initWithObjects:@"R", @"F", @"Uw", @"L", @"B", nil];
        turnr2 = [[NSArray alloc] initWithObjects:@"R", @"F", @"U", @"D", nil];
        suffr = [[NSArray alloc] initWithObjects:@"'", @"2", @"", nil];
        srand((unsigned)time(0));
    }
    return self;
}

bool iniRTow = false;
- (void)initRTow {
    if(iniRTow)return;
    initIm();
    int arr[8];
    int idx[] = {3,0,1,2};
    for (int i = 0; i < 40320; i++) {
        for (int j = 0; j < 6; j++) {
            set8Perm(arr, i);
            switch(j){
				case 0:cir(arr, 4, 5, 6, 7);break;	//D
				case 1:cir(arr, 1, 2, 6, 5);break;	//R
				case 2:cir(arr, 2, 3, 7, 6);break;	//F
				case 3:cir(arr, 0, 3, 2, 1);break;	//U
				case 4:cir(arr, 0, 4, 7, 3);break;	//L
				case 5:cir(arr, 0, 1, 5, 4);break;	//B
            }
            if(j>0)epmr[i][j-1]=get8Perm(arr);
            switch(j){
				case 1:cir(arr, 1, 2, 6, 5);break;	//R
				case 2:cir(arr, 2, 3, 7, 6);break;	//F
            }
            if(j<4)cpmr[i][idx[j]]=get8Perm(arr);
        }
    }
    for (int i = 0; i < 2187; i++) {
        for (int j = 0; j < 5; j++) {
            idxToZsOri(arr, i, 3, 8);
            switch(j){
				case 2:cir(arr, 0, 3, 2, 1);break;	//U
				case 0:cir(arr, 1, 2, 6, 5);
                    arr[1]=(arr[1]+1)%3;arr[2]=(arr[2]+2)%3;
                    arr[6]=(arr[6]+1)%3;arr[5]=(arr[5]+2)%3;
                    break;	//R
				case 1:cir(arr, 2, 3, 7, 6);
                    arr[2]=(arr[2]+1)%3;arr[3]=(arr[3]+2)%3;
                    arr[7]=(arr[7]+1)%3;arr[6]=(arr[6]+2)%3;
                    break;	//F
				case 3:cir(arr, 0, 4, 7, 3);
                    arr[3]=(arr[3]+1)%3;arr[0]=(arr[0]+2)%3;
                    arr[4]=(arr[4]+1)%3;arr[7]=(arr[7]+2)%3;
                    break;	//L
				case 4:cir(arr, 0, 1, 5, 4);
                    arr[0]=(arr[0]+1)%3;arr[1]=(arr[1]+2)%3;
                    arr[5]=(arr[5]+1)%3;arr[4]=(arr[4]+2)%3;
                    break;	//B
            }
            eomr[i][j]=zsOriToIdx(arr, 3, 8);
        }
    }
    for (int i = 1; i < 40320; i++)
        cpdr[i]=epdr[i]=-1;
    cpdr[0]=epdr[0]=0;
    //int nVisited=1;
    for(int d=0; d<13; d++) {
        //nVisited = 0;
        for (int i = 0; i < 40320; i++)
            if (cpdr[i] == d)
                for (int k = 0; k < 4; k++)
                    for(int y = i, m = 0; m < facesr[k]; m++) {
                        y = cpmr[y][k];
                        if (cpdr[y] < 0) {
                            cpdr[y] = d + 1;
                            //nVisited++;
                        }
                    }
        //System.out.println(d+1+" "+nVisited);
    }
    for(int d=0; d<7; d++) {
        //nVisited = 0;
        for (int i = 0; i < 40320; i++)
            if (epdr[i] == d)
                for (int k = 0; k < 5; k++)
                    for(int y = i, m = 0; m < 3; m++) {
                        y = epmr[y][k];
                        if (epdr[y] < 0) {
                            epdr[y] = d + 1;
                            //nVisited++;
                        }
                    }
        //System.out.println(d+" "+nVisited);
    }
    for (int i = 1; i < 2187; i++)
        eodr[i]=-1;
    eodr[0]=0;
    for(int d=0; d<6; d++) {
        //nVisited = 0;
        for (int i = 0; i < 2187; i++) 
            if (eodr[i] == d) {
                for (int k = 0; k < 5; k++)
                    for(int y = i, m = 0; m < 3; m++) {
                        y = eomr[y][k];
                        if (eodr[y] < 0) {
                            eodr[y] = d + 1;
                            //nVisited++;
                        }
                    }
            }
        //System.out.println(d+" "+nVisited);
    }
    iniRTow=true;
}

- (bool)search2:(int)cp ep:(int)ep d:(int)d lf:(int)lf {
    if (d == 0) return cp == 0 && ep == 0;
    if (epdr[ep] > d || cpdr[cp] > d) return false;
    for (int i = 0; i < 4; i++) {
        if (i != lf) {
            int y = cp, s = ep;
            for(int k = 0; k < facesr[i]; k++){
                y = cpmr[y][i]; if(i<2)s = epmr[epmr[s][i]][i];
                if([self search2:y ep:s d:d-1 lf:i]){
                    //sb.insert(0, turn2[i]+(i<2?"2":suff[k])+" ");
                    seqr[d + lenr1] = i*3+(i<2?1:k);
                    return true;
                }
            }
        }
    }
    return false;
}

- (NSString *)solve2:(int)cp lf:(int)lf {
    for(int i=lenr1; i>0; i--) {
        int t = seqr[i]/3, s = seqr[i]%3;
        cp = epmr[cp][t];
        if(s>0) {
            cp = epmr[cp][t];
            if(s>1) cp = epmr[cp][t];
        }
    }
    for (int depth = 0; ; depth++) {
        if([self search2:cp ep:0 d:depth lf:lf]) {
            NSMutableString *sb = [NSMutableString string];
            for(int i=lenr1+1; i<=depth+lenr1; i++) 
                [sb appendFormat:@"%@%@ ", [turnr2 objectAtIndex:seqr[i]/3], [suffr objectAtIndex:seqr[i]%3]];
                //sb.append(turn2[seq[i]/3]+suff[seq[i]%3]+" ");
            for(int i=1; i<=lenr1; i++)
                [sb appendFormat:@"%@%@ ", [turnr1 objectAtIndex:seqr[i]/3], [suffr objectAtIndex:seqr[i]%3]];
                //sb.append(turn1[seq[i]/3]+suff[seq[i]%3]+" ");
            return sb;
        }
    }
}

- (bool)search1:(int)ep eo:(int)eo d:(int)d lf:(int)lf {
    if (d == 0) return eo == 0 && ep == 0;
    if (epdr[ep] > d || eodr[eo] > d) return false;
    for (int i = 0; i < 5; i++) {
        if (i != lf) {
            int y=eo, s=ep;
            for(int j=0; j<3; j++){
                y=eomr[y][i]; s=epmr[s][i];
                if ([self search1:s eo:y d:d-1 lf:i]) {
                    //sb.insert(0, turn1[i]+suff[j]+" ");
                    seqr[d] = i*3+j;
                    return true;
                }
            }
        }
    }
    return false;
}

- (NSString *)scramble {
    [self initRTow];
    int eo = rand()%2187;
    int ep = rand()%40320;
    int cp = rand()%40320;
    for (int depth = 0; depth < 20; depth++) {
        if([self search1:ep eo:eo d:depth lf:-1]) {
            lenr1 = depth;
            int lf = seqr[1]/3;
            if(lf > 2) lf = -1;
            //System.out.print(sb.toString()+".");
            return [self solve2:cp lf:lf];
        }
    }
    return @"";
}
@end
