//
//  EOLine.m
//  DCTimer solvers
//
//  Created by MeigenChou on 13-4-5.
//  Copyright (c) 2013年 MeigenChou. All rights reserved.
//

#import "EOLine.h"
#import "Im.h"

@implementation EOLine
short eomEo[2048][6];
short epmEo[132][6];
char eodEo[2048];
char epdEo[132];
NSArray *eoFaceStr;
NSArray *eoMoveIdx;
NSArray *eoRotIdx;
NSArray *eoTurn;
NSArray *eoSuff;
NSMutableString *eoSol;
bool inieo = false;

- (id)init {
    if(self = [super init]) {
        eoFaceStr = [[NSArray alloc] initWithObjects:@"D(LR)", @"D(FB)", @"U(LR)", @"U(FB)",
                   @"L(UD)", @"L(FB)", @"R(UD)", @"R(FB)", @"F(LR)", @"F(UD)", @"B(LR)", @"B(UD)", nil];
        eoMoveIdx = [[NSArray alloc] initWithObjects:@"UDLRFB", @"UDFBRL", @"DURLFB", @"DUFBLR",
                   @"RLUDFB", @"RLFBDU", @"LRDUFB", @"LRFBUD", @"BFLRUD", @"BFUDRL", @"FBLRDU", @"FBDURL", nil];
        eoRotIdx = [[NSArray alloc] initWithObjects:@"", @"y", @"z2", @"z2 y",
                  @"z'", @"z' y", @"z", @"z y", @"x'", @"x' y", @"x", @"x y", nil];
        eoTurn = [[NSArray alloc] initWithObjects:@"U", @"D", @"L", @"R", @"F", @"B", nil];
        eoSuff = [[NSArray alloc] initWithObjects:@"", @"2", @"'", nil];
    }
    return self;
}

- (int)getEoEpm:(int)eci epi:(int)epi k:(int)k {
    bool comb[12];
    idxToComb(comb, eci, 2, 12);
    int perm[2];
    idxToPerm(perm, epi, 2);
    int selEdges[] = {8, 10};
    int next = 0;
    int ep[] = { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 };
    for (int i = 0; i < 12; i++)
        if (comb[i]) ep[i] = selEdges[perm[next++]];
    switch(k){
		case 0: cir(ep, 4, 7, 6, 5); break;
		case 1: cir(ep, 8, 9, 10, 11); break;
		case 2: cir(ep, 7, 3, 11, 2); break;
		case 3: cir(ep, 5, 1, 9, 0); break;
		case 4: cir(ep, 6, 2, 10, 1); break;
		case 5: cir(ep, 4, 0, 8, 3); break;
    }
    int edgesMap[] = {0, 1, 2, 3};
    bool ec[12];
    for (int i = 0; i < 12; i++)
        ec[i] = ep[i] > 0;
    eci = combToIdx(ec, 2, 12);
    int edgesPerm[] = {-1, -1};
    next = 0;
    for (int i = 0; i < 12; i++)
        if (ec[i]) {
            if(ep[i] > -1)
                edgesPerm[next++] 
                = edgesMap
                [ep[i]-8];
        }
    epi = permToIdx(edgesPerm, 2);
    return eci * 2 + epi;
}

- (void)initeo {
    if(inieo) return;
    int arr[12];
    for(int i=0; i<2048; i++){
        for(int j=0; j<6; j++) {
            idxToZsOri(arr, i, 2, 12);
            switch(j){
				case 0: cir(arr, 4, 7, 6, 5); break;
				case 1: cir(arr, 8, 9, 10, 11); break;
				case 2: cir(arr, 7, 3, 11, 2); break;
				case 3: cir(arr, 5, 1, 9, 0); break;
				case 4: cir(arr, 6, 2, 10, 1);
					arr[6]^=1; arr[2]^=1; arr[10]^=1; arr[1]^=1; break;
				case 5: cir(arr, 4, 0, 8, 3);
					arr[4]^=1; arr[0]^=1; arr[8]^=1; arr[3]^=1; break;
            }
            eomEo[i][j] = zsOriToIdx(arr, 2, 12);
        }
    }
    for(int i=0; i<66; i++){
        for(int j=0; j<2; j++){
            for(int k=0; k<6; k++){
                epmEo[i*2+j][k] = [self getEoEpm:i epi:j k:k];
            }
        }
        
    }
    for(int i=1; i<2048; i++) eodEo[i] = -1;
    eodEo[0] = 0;
    int d = 0;
    //int n = 1;
    for(d=0; d<7; d++){
        //n=0;
        for(int i=0; i<2048; i++)
            if(eodEo[i] == d)
                for(int j=0; j<6; j++)
                    for(int y=i,m=0; m<3; m++){
                        y=eomEo[y][j];
                        if(eodEo[y]==-1){
                            eodEo[y]=d+1;
                            //n++;
                        }
                    }
        //System.out.println(d+" "+n);
    }
    
    for(int i=0; i<132; i++) epdEo[i] = -1;
    epdEo[106] = 0;
    for(d=0; d<4; d++){
        //n=0;
        for(int i=0; i<132; i++)
            if(epdEo[i] == d)
                for(int j=0; j<6; j++){
                    int y=i;
                    for(int m=0; m<3; m++){
                        y=epmEo[y][j];
                        if(epdEo[y]==-1){
                            epdEo[y]=d+1;
                            //n++;
                        }
                    }
                }
        //System.out.println(d+" "+n);
    }
    inieo=true;
}

- (bool)search:(int)eo ep:(int)ep d:(int)depth l:(int)l {
    if(depth==0) return eo==0 && ep==106;
    if(eodEo[eo]>depth || epdEo[ep]>depth) return false;
    for (int i = 0; i < 6; i++)
        if (i != l) {
            int w = eo, y = ep;
            for (int j = 0; j < 3; j++) {
                y = epmEo[y][i];
                w = eomEo[w][i];
                if ([self search:w ep:y d:depth-1 l:i]) {
                    [eoSol insertString:[NSString stringWithFormat:@" %@%@", [eoTurn objectAtIndex:i], [eoSuff objectAtIndex:j]] atIndex:0];
                    //sb.insert(0, " " + turn[i] + suff[j]);
                    return true;
                }
            }
        }
    return false;
}

- (NSString *)solveEo:(NSString *)scr side:(int)side {
    NSArray *s = [scr componentsSeparatedByString:@" "];
    int ep = 106, eo = 0;
    for(int d=0; d<s.count; d++) {
        if([[s objectAtIndex:d] length]!=0) {
            char i = [[s objectAtIndex:d] characterAtIndex:0];
            NSString *idx = [NSString stringWithFormat:@"%c", i];
            NSRange rang = [[eoMoveIdx objectAtIndex:side] rangeOfString:idx];
            int o = rang.location;
            ep = epmEo[ep][o]; eo = eomEo[eo][o];
            if([[s objectAtIndex:d] length]>1) {
                i = [[s objectAtIndex:d] characterAtIndex:1];
                if(i == '2') {
                    eo = eomEo[eo][o]; ep = epmEo[ep][o];
                } else {
                    eo = eomEo[eomEo[eo][o]][o]; ep = epmEo[epmEo[ep][o]][o];
                }
            }
        }
    }
    eoSol = [NSMutableString string];
    for (int d = 0; ![self search:eo ep:ep d:d l:side]; d++);
    return [NSString stringWithFormat:@"%@:%@%@", [eoFaceStr objectAtIndex:side], [eoRotIdx objectAtIndex:side], eoSol];
}

- (NSString *)eoLine:(NSString *)scr side:(int)side {
    [self initeo];
    return [NSString stringWithFormat:@"%@\n%@", [self solveEo:scr side:side*2], [self solveEo:scr side:side*2+1]];
}
@end
