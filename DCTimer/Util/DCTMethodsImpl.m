//
//  DCTMethodsImpl.m
//  DCTimer
//
//  Created by Chou Meigen on 13-3-15.
//  Copyright (c) 2013年 __MyCompanyName__. All rights reserved.
//

#import "DCTMethodsImpl.h"

//@interface DCTMethodsImpl()
//@end

@implementation DCTMethodsImpl

NSMutableArray *resList, *penList, *scrList, *dateList;
const int MAX_VALUE = 2147483647;
const int MIN_VALUE = -2147483647;
int maxIdx, minIdx;
int sessionMean, sessionSD;
int sessionAvg, sessionASD;
int numcube=0, solved=0;
int curAvg[4], bestAvg[4], bestAvgIdx[4];
int numOfAvg[4] = {5, 12, 50, 100};
int curMean3, bestMean3, bestMeanIdx;
extern int accuracy;
extern bool clkFormat;
extern bool prntScr;

- (id) init {
    if(self = [super init]) {
        if(!resList) {
            resList = [[NSMutableArray alloc] init];
            penList = [[NSMutableArray alloc] init];
            scrList = [[NSMutableArray alloc] init];
            dateList = [[NSMutableArray alloc] init];
        }
    }
    return self;
}

NSString *contime(int hour, int min, int sec, int msec) {
    NSMutableString *time = [NSMutableString string];
    if(hour==0) {
        if(min==0) [time appendFormat:@"%d", sec];
        else {
            if(sec<10) [time appendFormat:@"%d:0%d", min, sec];
            else [time appendFormat:@"%d:%d", min, sec];
        }
    }
    else {
        [time appendFormat:@"%d%@%d%@%d", hour, min<10?@":0":@":", min, sec<10?@":0":@":", sec];
    }
    if(accuracy == 0) {
        if(msec<10) [time appendFormat:@".00%d", msec];
        else if(msec<100) [time appendFormat:@".0%d", msec];
        else [time appendFormat:@".%d", msec];
    } else {
        if(msec<10) [time appendFormat:@".0%d", msec];
        else [time appendFormat:@".%d", msec];
    }
    return time;
}

NSString *distime(int i) {
    bool m = i<0;
    if(m)i = -i;
    if(accuracy == 1)i+=5;
    int msec=i%1000;
    if(accuracy == 1)msec/=10;
    int sec=clkFormat?(i/1000)%60:i/1000;
    int min=clkFormat?(i/60000)%60:0;
    int hour=clkFormat?i/3600000:0;
    NSString *time = [[NSString alloc] initWithFormat:@"%@%@", m?@"-":@"", contime(hour, min, sec, msec)];
    return time;
}

- (NSString *)distimeAtIndex:(int)idx dt:(bool)d {
    int p = [[penList objectAtIndex:idx] intValue];
    int r = [[resList objectAtIndex:idx] intValue];
    if(p==2) {
        if(d) return [NSString stringWithFormat:@"DNF (%@)", distime(r)];
        else return @"DNF";
    }
    else if(p==1)
        return [NSString stringWithFormat:@"%@+", distime(r+2000)];
    else return distime(r);
}

- (void)addTime:(int)time penalty:(int)pen scramble:(NSString *)scr datetime:(NSString *)dt {
    [resList addObject:[NSNumber numberWithInt:time]];
    [penList addObject:[NSNumber numberWithChar:pen]];
    [scrList addObject:scr];
    [dateList addObject:dt];
    //NSLog(@"%d", resList.count);
}

- (void)clearTime {
    [resList removeAllObjects];
    [penList removeAllObjects];
    [scrList removeAllObjects];
    [dateList removeAllObjects];
}

- (int)numberOfSolves {
    return resList.count;
}

- (NSString *)cubeSolves {
    return [NSString stringWithFormat:@"%d/%d", solved, numcube];
}

- (int)getPenaltyAtIndex:(int)idx {
    return [[penList objectAtIndex:idx] intValue];
}

- (void)setPenalty:(int)pen atIndex:(int)idx {
    [penList replaceObjectAtIndex:idx withObject:[NSNumber numberWithInt:pen]];
    //[[resList objectAtIndex:idx] replaceObjectAtIndex:1 withObject:[NSNumber numberWithInt:pen]];
}

- (NSString *)getScrambleAtIndex:(int)idx {
    return [scrList objectAtIndex:idx];
}

- (NSString *)getDateAtIndex:(int)idx {
    return [dateList objectAtIndex:idx];
}

- (void)deleteTimeAtIndex:(int)idx {
    [resList removeObjectAtIndex:idx];
    [penList removeObjectAtIndex:idx];
    [scrList removeObjectAtIndex:idx];
    [dateList removeObjectAtIndex:idx];
}

- (void)getSessionStats {
    numcube = resList.count;
    maxIdx = minIdx = sessionSD = -1;
    if(numcube==0) return;
    int min = MAX_VALUE;
    int max = MIN_VALUE;
    solved = 0;
    double sum = 0, sum2 = 0;
    for(int i=0; i<numcube; i++) {
        int p = [[penList objectAtIndex:i] intValue];
        if(p!=2) {
            solved++;
            int time = [[resList objectAtIndex:i] intValue] + 2000 * p;
            if(time<=min) {
                min = time;
                minIdx = i;
            }
            if(time>max) {
                max = time;
                maxIdx = i;
            }
            if(accuracy==0) {
                sum += time;
                sum2 += pow(time, 2);
            } else {
                sum += (time+5)/10;
                sum2 += pow((time+5)/10, 2);
            }
        }
    }
    if(minIdx == -1) minIdx = numcube - 1;
    if(maxIdx == -1) maxIdx = 0;
    sessionMean=(int)(sum/solved+0.5);
    if(accuracy==1)sessionMean*=10;
    sessionSD=(int)(sqrt((sum2-sum*sum/solved)/solved));
}

- (void)getSessionAvg {
    if(numcube > 2) {
        NSMutableArray *data = [[NSMutableArray alloc] init];
        for(int i=0; i<numcube; i++) {
            int p = [[penList objectAtIndex:i] intValue];
            if(p!=2) {
                int r = [[resList objectAtIndex:i] intValue] + 2000 * p;
                [data addObject:[NSNumber numberWithInt:r]];
            }
        }
        int trimmed = ceil(numcube/20.0);
        if(data.count < numcube - trimmed) {
            sessionAvg = MAX_VALUE;
        } else {
            double sum = 0, sum2 = 0;
            [data sortUsingSelector:@selector(compare:)];
            for(int j=trimmed; j<numcube-trimmed; j++) {
                int time = [[data objectAtIndex:j] intValue];
                if(accuracy==0) {
                    sum+=time;
                    sum2+=pow(time, 2);
                } else {
                    sum+=(time+5)/10;
                    sum2+=pow((time+5)/10, 2);
                }
            }
            int num = numcube - 2*trimmed;
            sessionAvg = (int)(sum/num+0.5);
            if(accuracy==1)sessionAvg*=10;
            sessionASD = (int)(sqrt((sum2-sum*sum/num)/num));
        }
    }
}

- (void)getAvgs:(int)idx {
    int num = numOfAvg[idx];
    bestAvgIdx[idx] = -1;
    bestAvg[idx] = MAX_VALUE;
    double sum = 0;
    if(numcube >= num) {
        int cavg;
        for(int i=num-1; i<numcube; i++) {
            int nDnf=0;
            sum = 0;
            int max = MIN_VALUE;
            int min = MAX_VALUE;
            for(int j = i-num+1; j<=i; j++) {
                int p = [[penList objectAtIndex:j] intValue];
                int r = [[resList objectAtIndex:j] intValue]+2000*p;
                if(p==2) {
                    nDnf++;
                    max = MAX_VALUE;
                }
                else {
                    if(r > max) max = r;
                    if(r < min) min = r;
                    if(accuracy==0) sum += r;
                    else sum += (r+5)/10;
                }
            }
            if(nDnf>1){
                cavg = MAX_VALUE;
                continue;
            }
            if(nDnf!=0) max = 0;
            if(accuracy==1) {
                max = (max+5)/10;
                min = (min+5)/10;
            }
            sum -= min+max;
            cavg = (int)(sum/(num-2)+0.5);
            if(accuracy==1) cavg*=10;
            if(i==num-1 || cavg <= bestAvg[idx]) {
                bestAvg[idx] = cavg; bestAvgIdx[idx] = i;
            }
        }
        //cavg = (int)(sum/(num-2)+0.5);
        //if(accuracy==1) cavg*=10;
        curAvg[idx] = cavg;
        //bestAvg[idx] = (int)(bestSum[idx]/(num-2)+0.5);
        if(accuracy==1) bestAvg[idx]*=10;
    }
}

- (void)quickSort:(NSMutableArray *)a l:(int)lo0 h:(int)hi0 {
    int lo = lo0, hi = hi0;
    if (lo >= hi) return;
    bool transfer=true;
    while (lo != hi) {
        int alo = [[a objectAtIndex:lo] intValue], ahi = [[a objectAtIndex:hi] intValue];
        if (alo > ahi) {
            NSNumber *temp = [a objectAtIndex:lo];
            [a replaceObjectAtIndex:lo withObject:[a objectAtIndex:hi]];
            [a replaceObjectAtIndex:hi withObject:temp];
            transfer = !transfer;
        }
        if(transfer) hi--;
        else lo++;
    }
    lo--; hi++;
    [self quickSort:a l:lo0 h:lo];
    [self quickSort:a l:hi h:hi0];
}

- (void)getAvgs20:(int)idx {
    int num = numOfAvg[idx];
    double sum = 0;
    bestAvgIdx[idx] = -1;
    bestAvg[idx] = MAX_VALUE;
    int trimmed = ceil(num/20.0);
    if(numcube >= num) {
        int cavg;
        for(int i=num-1; i<numcube; i++) {
            int nDnf=0;
            sum = 0;
            for(int j = i-num+1; j<=i; j++) {
                int p = [[penList objectAtIndex:j] intValue];
                if(p==2)nDnf++;
            }
            if(nDnf>trimmed){
                cavg = MAX_VALUE;
                continue;
            }
            NSMutableArray *data = [[NSMutableArray alloc]init];
            for(int j = i-num+1; j<=i; j++) {
                int p = [[penList objectAtIndex:j] intValue];
                if(p!=2) [data addObject:[NSNumber numberWithInt:[[resList objectAtIndex:j] intValue]+2000*p]];
            }
            [self quickSort:data l:0 h:data.count-1];
            //[data sortUsingSelector:@selector(compare:)];
            for(int j=trimmed; j<num-trimmed; j++) {
                sum += [[data objectAtIndex:j] intValue];
            }
            cavg = (int)(sum/(num-2*trimmed)+0.5);
            if(i==num-1 || cavg <= bestAvg[idx]) {
                bestAvg[idx] = cavg; bestAvgIdx[idx] = i;
            }
        }
        curAvg[idx] = cavg;
    }
}

- (NSString *)bestTime {
    return [self distimeAtIndex:minIdx dt:false];
}

- (NSString *)worstTime {
    return [self distimeAtIndex:maxIdx dt:false];
}

- (int) getMaxIndex {
    return maxIdx;
}

- (int) getMinIndex {
    return minIdx;
}

- (NSString *)currentAvg:(int)idx {
    int ca = curAvg[idx];
    if(ca==MAX_VALUE)return @"DNF";
    return distime(ca);
}

- (NSString *)bestAvg:(int)idx {
    if(bestAvgIdx[idx]==-1)return @"DNF";
    return distime(bestAvg[idx]);
}

- (int) bestAvgIdx:(int)idx {
    int bai = bestAvgIdx[idx];
    if(bai == -1) return numcube-1;
    return bai;
}

- (NSString *)standDev:(int)i {
    if(i<0)return @"N/A";
    if(accuracy==0)i=(i+5)/10;
    return [NSString stringWithFormat:@"%d.%@%d", i/100, (i%100<10)?@"0":@"", i%100];
}

- (NSString *)getSessionMeanSD {
    if(solved==0) return @"N/A";
    else return [NSString stringWithFormat:@"%@ (σ = %@)", distime(sessionMean), [self standDev:sessionSD]];
}

- (NSString *)getSessionAvgSD {
    if(numcube < 3) return @"N/A";
    if(sessionAvg == MAX_VALUE) return @"DNF";
    return [NSString stringWithFormat:@"%@ (σ = %@)", distime(sessionAvg), [self standDev:sessionASD]];
}

- (void)getMean:(int)num {
    bestMeanIdx = -1;
    bestMean3 = MAX_VALUE;
    if(numcube >= num) {
        int cavg;
        for(int i=num-1; i<numcube; i++) {
            int nDnf=0;
            double sum = 0;
            for(int j = i-num+1; j<=i; j++) {
                int p = [[penList objectAtIndex:j] intValue];
                int r = [[resList objectAtIndex:j] intValue]+2000*p;
                if(p==2)nDnf++;
                else {
                    if(accuracy==0) sum+=r;
                    else sum+=(r+5)/10;
                }
            }
            if(nDnf>0){
                cavg = MAX_VALUE;
                continue;
            }
            cavg = (int)(sum/num+0.5);
            if(accuracy==1)cavg*=10;
            if(i==num-1 || cavg <= bestMean3) {
                bestMean3 = cavg; bestMeanIdx = i;
            }
        }
        curMean3 = cavg;
    }
}

- (NSString *)currentMean3 {
    if(curMean3==MAX_VALUE)return @"DNF";
    return distime(curMean3);
}

- (NSString *)getBestMean3 {
    if(bestMeanIdx==-1)return @"DNF";
    return distime(bestMean3);
}

- (int)getBestMeanIdx {
    if(bestMeanIdx==-1)return numcube-1;
    return bestMeanIdx;
}

- (NSString *)getMsgOfAvg:(int)idx num:(int)n {
    NSMutableString *s = [NSMutableString stringWithString:NSLocalizedString(@"stat_title", @"")];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd"];
    NSString *date = [formatter stringFromDate:[NSDate date]];
    [s appendFormat:@"%@\n%@", date, NSLocalizedString(@"stat_avg", @"")];
    int maxi=-1,mini=-1,dnf=0;
    int max = MIN_VALUE, min = MAX_VALUE;
    int cavg=0, csdv=-1,ind=1;
    double sum=0,sum2=0;
    bool m=false;
    for(int j=idx-n+1;j<=idx;j++) {
        int p = [[penList objectAtIndex:j] intValue];
        int r = [[resList objectAtIndex:j] intValue]+2000*p;
        if(p==2) {
            dnf++;
            max = MAX_VALUE;
            maxi = j;
        }
        else {
            if(r > max) {
                max = r;
                maxi = j;
            }
            if(r <= min) {
                min = r;
                mini = j;
            }
            if(accuracy==0) {
                sum+=r;
                sum2+=pow(r, 2);
            }
            else {
                sum+=(r+5)/10;
                sum2+=pow((r+5)/10, 2);
            }
        }
    }
    m = dnf>1;
    if(m) {
        if(maxi==-1)maxi = idx-n+1;
        if(mini==-1)mini = idx;
        cavg = csdv = 0;
    } else {
        if(dnf!=0) max = 0;
        if(accuracy==1) {
            max = (max+5)/10;
            min = (min+5)/10;
        }
        sum -= min+max;
        sum2 -= pow(min, 2)+pow(max, 2);
        cavg = sum/(n-2)+0.5;
        if(accuracy==1)cavg*=10;
        csdv = sqrt(sum2/(n-2)-sum*sum/(n-2)/(n-2))+(accuracy?0.5:0);
    }
    [s appendFormat:@"%@ (σ = %@)\n", m?@"DNF":distime(cavg), m?@"N/A":[self standDev:csdv]];
    [s appendFormat:@"%@%@\n", NSLocalizedString(@"stat_best", @""), [self distimeAtIndex:mini dt:false]];
    [s appendFormat:@"%@%@\n%@", NSLocalizedString(@"stat_worst", @""), [self distimeAtIndex:maxi dt:false], NSLocalizedString(@"stat_list", @"")];
    if(!prntScr)[s appendString:@"\n"];
    for(int j=idx-n+1;j<=idx;j++) {
        if(prntScr)[s appendFormat:@"\n%d. ", ind++];
        if(j==mini || j==maxi) [s appendString:@"("];
        [s appendString:[self distimeAtIndex:j dt:false]];
        if(j==mini || j==maxi) [s appendString:@")"];
        if(!prntScr && j<idx)[s appendString:@", "];
        if(prntScr) [s appendFormat:@"  %@", [scrList objectAtIndex:j]];
    }
    return s;
}

- (NSString *)getMsgOfMean3:(int)idx {
    NSMutableString *s = [NSMutableString stringWithString:NSLocalizedString(@"stat_title", @"")];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd"];
    NSString *date = [formatter stringFromDate:[NSDate date]];
    [s appendFormat:@"%@\n%@", date, NSLocalizedString(@"stat_mean", @"")];
    int maxi=-1,mini=-1,dnf=0;
    int max = MIN_VALUE, min = MAX_VALUE;
    int cavg=0, csdv=-1,ind=1;
    double sum=0,sum2=0;
    bool m=false;
    for(int j=idx-2;j<=idx;j++) {
        int p = [[penList objectAtIndex:j] intValue];
        int r = [[resList objectAtIndex:j] intValue]+2000*p;
        if(p==2) {
            dnf++;
            max = MAX_VALUE;
            maxi = j;
        }
        else {
            if(r > max) {
                max = r;
                maxi = j;
            }
            if(r <= min) {
                min = r;
                mini = j;
            }
            if(accuracy==0) {
                sum+=r;
                sum2+=pow(r, 2);
            }
            else {
                sum+=(r+5)/10;
                sum2+=pow((r+5)/10, 2);
            }
        }
    }
    m = dnf>0;
    if(m) {
        if(maxi==-1)maxi = idx-2;
        if(mini==-1)mini = idx;
        cavg = csdv = 0;
    } else {
        cavg = sum/3+0.5;
        if(accuracy==1)cavg*=10;
        csdv = sqrt(sum2/3-sum*sum/9)+(accuracy?0.5:0);
    }
    [s appendFormat:@"%@ (σ = %@)\n", m?@"DNF":distime(cavg), m?@"N/A":[self standDev:csdv]];
    [s appendFormat:@"%@%@\n", NSLocalizedString(@"stat_best", @""), [self distimeAtIndex:mini dt:false]];
    [s appendFormat:@"%@%@\n%@", NSLocalizedString(@"stat_worst", @""), [self distimeAtIndex:maxi dt:false], NSLocalizedString(@"stat_list", @"")];
    if(!prntScr)[s appendString:@"\n"];
    for(int j=idx-2;j<=idx;j++) {
        if(prntScr)[s appendFormat:@"\n%d. ", ind++];
        [s appendString:[self distimeAtIndex:j dt:false]];
        if(!prntScr && j<idx)[s appendString:@", "];
        if(prntScr) [s appendFormat:@"  %@", [scrList objectAtIndex:j]];
    }
    return s;
}

- (NSString *)getSessionMean {
    NSMutableString *s = [NSMutableString stringWithString:NSLocalizedString(@"stat_title", @"")];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd"];
    NSString *date = [formatter stringFromDate:[NSDate date]];
    [s appendFormat:@"%@\n%@", date, NSLocalizedString(@"stat_solve", @"")];
    [s appendFormat:@"%d/%d\n", solved, numcube];
    [s appendFormat:@"%@%@\n", NSLocalizedString(@"ses_mean", @""), [self getSessionMeanSD]];
    [s appendFormat:@"%@%@\n", NSLocalizedString(@"ses_avg", @""), [self getSessionAvgSD]];
    [s appendFormat:@"%@%@\n", NSLocalizedString(@"stat_best", @""), [self bestTime]];
    [s appendFormat:@"%@%@\n%@", NSLocalizedString(@"stat_worst", @""), [self worstTime], NSLocalizedString(@"stat_list", @"")];
    if(!prntScr)[s appendString:@"\n"];
    for(int j=0; j<numcube; j++) {
        if(prntScr)[s appendFormat:@"\n%d. ", j+1];
        [s appendString:[self distimeAtIndex:j dt:false]];
        if(!prntScr && j<numcube-1)[s appendString:@", "];
        if(prntScr) [s appendFormat:@"  %@", [scrList objectAtIndex:j]];
    }
    return s;
}

@end
