//
//  DCTDbHelper.m
//  DCTimer
//
//  Created by MeigenChou on 13-3-29.
//  Copyright (c) 2013年 MeigenChou. All rights reserved.
//

#import "DCTDbHelper.h"
#import <sqlite3.h>
#import "DCTMethodsImpl.h"

@interface DCTDbHelper ()
@property (nonatomic, strong) DCTMethodsImpl *mi;
@end

@implementation DCTDbHelper
@synthesize mi = _mi;
sqlite3 *database;
int dbLastId = 0;
int sesLastId = 0;
NSMutableArray *resultId;
NSMutableArray *sesData;
int crntSesId = 0;

- (DCTMethodsImpl *)mi {
    if(!_mi) 
        _mi = [[DCTMethodsImpl alloc] init];
    return _mi;
}

- (NSString *)dataFilePath {
    NSArray *paths=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docDircty = [paths objectAtIndex:0];
    return [docDircty stringByAppendingPathComponent:@"spdcube.sqlite"];
}

- (NSString *) openDB {
    if(sqlite3_open([[self dataFilePath] UTF8String], &database) != SQLITE_OK) {
        //sqlite3_close(database);
        return @"fopn";
    }
    NSString *createSQL = @"create table if not exists sessiontb (rowid integer, name text)";
    char *errorMsg;
    if(sqlite3_exec(database, [createSQL UTF8String], NULL, NULL, &errorMsg) != SQLITE_OK) {
        //sqlite3_close(database);
        NSLog(@"failed %s", errorMsg);
        return @"fctb";
    }
    createSQL = @"create table if not exists resulttb (id integer, sesid integer, rest integer, resp integer, scr text, date text, note text)";
    if(sqlite3_exec(database, [createSQL UTF8String], NULL, NULL, &errorMsg) != SQLITE_OK) {
        //sqlite3_close(database);
        NSLog(@"failed %s", errorMsg);
        return @"fctb";
    }
    return @"OK";
}

- (void)getSessions {
    NSString *msg = [self openDB];
    if([msg hasPrefix:@"f"]){
        NSLog(@"failed open");
        return;
    }
    NSString *query = @"select rowid, name from sessiontb";
    sqlite3_stmt *statement;
    if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
        int row = 0;
        sesData = [[NSMutableArray alloc] init];
        while (sqlite3_step(statement) == SQLITE_ROW) {
            row = sqlite3_column_int(statement, 0);
            char *na = (char *)sqlite3_column_text(statement, 1);
            NSString *name = [[NSString alloc] initWithUTF8String:na];
            [sesData addObject:[[NSArray alloc] initWithObjects:[NSNumber numberWithInt:row], name, nil]];
        }
        sesLastId = row;
        NSLog(@"session:%d", sesData.count);
        sqlite3_finalize(statement);
    }
}

- (void)getSessionName:(NSMutableArray *)ses {
    if(sesData.count!=0) {
        for(int i=0; i<sesData.count; i++) {
            [ses addObject:[[sesData objectAtIndex:i] objectAtIndex:1]];
        }
    }
}

- (void)query:(int)sesIdx {
    if(sesIdx!=0) sesIdx = [[[sesData objectAtIndex:sesIdx-1] objectAtIndex:0] intValue];
    crntSesId = sesIdx;
    [self.mi clearTime];
    NSString *query = @"select id, sesid, rest, resp, scr, date from resulttb";
    sqlite3_stmt *statement;
    if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
        int row = 0;
        resultId = [[NSMutableArray alloc] init];
        while (sqlite3_step(statement) == SQLITE_ROW) {
            row = sqlite3_column_int(statement, 0);
            int ses = sqlite3_column_int(statement, 1);
            if(ses == sesIdx) {
                int rest = sqlite3_column_int(statement, 2);
                int resp = sqlite3_column_int(statement, 3);
                char *sc = (char *)sqlite3_column_text(statement, 4);
                char *da = (char *)sqlite3_column_text(statement, 5);
                NSString *scr = [[NSString alloc] initWithUTF8String:sc];
                NSString *date = [[NSString alloc] initWithUTF8String:da];
                [self.mi addTime:rest penalty:resp scramble:scr datetime:date];
                [resultId addObject:[NSNumber numberWithInt:row]];
            }
        }
        dbLastId = row;
        NSLog(@"data:%d lastId:%d", resultId.count, dbLastId);
        sqlite3_finalize(statement);
    }
}

- (void)insertTime:(int)time pen:(int)pen scr:(NSString *)s date:(NSString *)da {
    [resultId addObject:[NSNumber numberWithInt:++dbLastId]];
    char *update = "insert into resulttb (id, sesid, rest, resp, scr, date) values (?, ?, ?, ?, ?, ?);";
    sqlite3_stmt *stmt;
    if(sqlite3_prepare_v2(database, update, -1, &stmt, nil) == SQLITE_OK) {
        sqlite3_bind_int(stmt, 1, dbLastId);
        sqlite3_bind_int(stmt, 2, crntSesId);
        sqlite3_bind_int(stmt, 3, time);
        sqlite3_bind_int(stmt, 4, pen);
        sqlite3_bind_text(stmt, 5, [s UTF8String], -1, NULL);
        sqlite3_bind_text(stmt, 6, [da UTF8String], -1, NULL);
    }
    if(sqlite3_step(stmt) != SQLITE_DONE) {
        NSLog(@"failed insert");
    }
    sqlite3_finalize(stmt);
}

- (void)addSession:(NSString *)name {
    [sesData addObject:[[NSArray alloc] initWithObjects:[NSNumber numberWithInt:++sesLastId], name, nil]];
    char *insert = "insert into sessiontb (rowid, name) values (?, ?);";
    sqlite3_stmt *stmt;
    if(sqlite3_prepare_v2(database, insert, -1, &stmt, nil) == SQLITE_OK) {
        sqlite3_bind_int(stmt, 1, sesLastId);
        sqlite3_bind_text(stmt, 2, [name UTF8String], -1, NULL);
    }
    if(sqlite3_step(stmt) != SQLITE_DONE) {
        NSLog(@"failed insert");
    }
    sqlite3_finalize(stmt);
}

- (void)deleteTime:(int)idx {
    char *delete = "delete from resulttb where id=?";
    sqlite3_stmt *stmt;
    if(sqlite3_prepare_v2(database, delete, -1, &stmt, nil) == SQLITE_OK) {
        sqlite3_bind_int(stmt, 1, [[resultId objectAtIndex:idx] intValue]);
    }
    if(sqlite3_step(stmt) != SQLITE_DONE) {
        NSLog(@"failed delete");
    }
    sqlite3_finalize(stmt);
    [resultId removeObjectAtIndex:idx];
}

- (void)deleteSession:(int)idx {
    if(idx!=0) {
        idx = [[[sesData objectAtIndex:idx-1] objectAtIndex:0] intValue];
        [sesData removeObjectAtIndex:idx-1];
    }
    char *delete = "delete from sessiontb where rowid=?";
    sqlite3_stmt *stmt;
    if(sqlite3_prepare_v2(database, delete, -1, &stmt, nil) == SQLITE_OK) {
        sqlite3_bind_int(stmt, 1, idx);
    }
    if(sqlite3_step(stmt) != SQLITE_DONE) {
        NSLog(@"failed delete");
    }
    sqlite3_finalize(stmt);
}

- (void)updateTime:(int)idx pen:(int)pen {
    char *update = "update resulttb set resp=? where id=?";
    sqlite3_stmt *stmt;
    if(sqlite3_prepare_v2(database, update, -1, &stmt, nil) == SQLITE_OK) {
        sqlite3_bind_int(stmt, 1, pen);
        sqlite3_bind_int(stmt, 2, [[resultId objectAtIndex:idx] intValue]);
    }
    if(sqlite3_step(stmt) != SQLITE_DONE) {
        NSLog(@"failed update");
    }
    sqlite3_finalize(stmt);
}

- (void)updateSession:(int)idx name:(NSString *)name {
    NSArray *temp = [[NSArray alloc] initWithObjects:[[sesData objectAtIndex:idx-1] objectAtIndex:0], name, nil];
    [sesData replaceObjectAtIndex:idx-1 withObject:temp];
    if(idx!=0) idx = [[[sesData objectAtIndex:idx-1] objectAtIndex:0] intValue];
    char *update = "update sessiontb set name=? where rowid=?";
    sqlite3_stmt *stmt;
    if(sqlite3_prepare_v2(database, update, -1, &stmt, nil) == SQLITE_OK) {
        sqlite3_bind_text(stmt, 1, [name UTF8String], -1, NULL);
        sqlite3_bind_int(stmt, 2, idx);
    }
    if(sqlite3_step(stmt) != SQLITE_DONE) {
        NSLog(@"failed update");
    }
    sqlite3_finalize(stmt);
}

- (void)clearSession:(int)sesIdx {
    if(sesIdx!=0) sesIdx = [[[sesData objectAtIndex:sesIdx-1] objectAtIndex:0] intValue];
    char *delete = "delete from resulttb where sesid=?";
    sqlite3_stmt *stmt;
    if(sqlite3_prepare_v2(database, delete, -1, &stmt, nil) == SQLITE_OK) {
        sqlite3_bind_int(stmt, 1, sesIdx);
    }
    if(sqlite3_step(stmt) != SQLITE_DONE) {
        NSLog(@"failed clear");
    }
    sqlite3_finalize(stmt);
    [self.mi clearTime];
}

- (void) closeDB {
    sqlite3_close(database);
}
@end
