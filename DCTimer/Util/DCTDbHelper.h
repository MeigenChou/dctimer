//
//  DCTDbHelper.h
//  DCTimer
//
//  Created by MeigenChou on 13-3-29.
//  Copyright (c) 2013年 MeigenChou. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DCTMethodsImpl.h"

@interface DCTDbHelper : NSObject
- (void)getSessions;
- (void)getSessionName:(NSMutableArray *)ses;
- (void)query:(int)sesIdx;
- (void)insertTime:(int)time pen:(int)pen scr:(NSString *)s date:(NSString *)da;
- (void)addSession:(NSString *)name;
- (void)deleteTime:(int)idx;
- (void)deleteSession:(int)idx;
- (void)clearSession:(int)sesIdx;
- (void)updateTime:(int)idx pen:(int)pen;
- (void)updateSession:(int)idx name:(NSString *)name;
- (void)closeDB;
@end
