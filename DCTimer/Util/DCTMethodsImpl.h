//
//  DCTMethodsImpl.h
//  DCTimer
//
//  Created by Chou Meigen on 13-3-15.
//  Copyright (c) 2013年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DCTDbHelper.h"

@interface DCTMethodsImpl : NSObject
NSString *distime(int i);
- (NSString *)distimeAtIndex:(int)idx dt:(bool)d;
- (void)addTime:(int)time penalty:(int)pen scramble:(NSString *)scr datetime:(NSString *)dt;
- (void)clearTime;
- (int)numberOfSolves;
- (NSString *)getScrambleAtIndex:(int)idx;
- (int)getPenaltyAtIndex:(int)idx;
- (void)setPenalty:(int)pen atIndex:(int)idx;
- (NSString *)getDateAtIndex:(int)idx;
- (void)deleteTimeAtIndex:(int)idx;
- (void)getSessionStats;
- (NSString *)cubeSolves;
- (NSString *)getSessionMeanSD;
- (NSString *)bestTime;
- (NSString *)worstTime;
- (void)getAvgs:(int)idx;
- (NSString *)currentAvg:(int)idx;
- (NSString *)bestAvg:(int)idx;
- (int) bestAvgIdx:(int)idx;
- (void)getMean:(int)num;
- (NSString *)currentMean3;
- (NSString *)getBestMean3;
- (int)getBestMeanIdx;
- (int) getMaxIndex;
- (int) getMinIndex;
- (void)getSessionAvg;
- (NSString *)getSessionAvgSD;
- (void)getAvgs20:(int)idx;
- (NSString *)getMsgOfAvg:(int)idx num:(int)n;
- (NSString *)getMsgOfMean3:(int)idx;
- (NSString *)getSessionMean;
@end
