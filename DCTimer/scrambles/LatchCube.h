//
//  LatchCube.h
//  DCTimer scramblers
//
//  Created by MeigenChou on 13-3-3.
//  Copyright (c) 2013年 MeigenChou. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LatchCube : NSObject

- (NSString *) scramble;

@end
