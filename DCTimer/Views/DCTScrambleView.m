//
//  DCTScrambleView.m
//  DCTimer
//
//  Created by MeigenChou on 13-4-8.
//
//

#import "DCTScrambleView.h"

@implementation DCTScrambleView
@synthesize curntColor;

- (id)initWithCoder:(NSCoder *)coder
{
    if(self = [super initWithCoder:coder]) {
        curntColor = [UIColor orangeColor];
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, 2.0);
    CGContextSetStrokeColorWithColor(context, curntColor.CGColor);
    CGRect curntRect = CGRectMake(10, 10, 50, 50);
    CGContextAddRect(context, curntRect);
    CGContextDrawPath(context, kCGPathFillStroke);
}

@end
