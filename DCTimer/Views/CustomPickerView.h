//
//  CustomPickerView.h
//  DCTimer
//
//  Created by Jichao Li on 5/28/12.
//  Copyright (c) 2012 Sufflok University. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomPickerView : UIAlertView  <UIPickerViewDataSource, UIPickerViewDelegate> 
{  
    NSDictionary *scrType;
    NSArray *types;  
    NSArray *subsets;  
    UIPickerView *selectPicker;  
    int selectedType;  
    int selectedSubset;  
}  

@property (nonatomic, strong) NSNumber *selScrType;
@property (nonatomic, strong) NSString *selScrName;

- (NSString *)getScrName:(int)idx;

@end
